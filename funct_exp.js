var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
//var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 35, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];


//Получаем массив со студентами

var students = studentsAndPoints.filter(function (student, index) {
	if (index % 2 == 0) {
		return true;
	};
});
console.log(students);


//Получаем массив с баллами 


var points = studentsAndPoints.filter(function (point, index) {
	if (index % 2 !== 0) {
		return true;
	};
});
console.log(points);


//Выводим список студентов с баллами

console.log('Список студентов:');
students.forEach(function (student, index) {	
	console.log('Студент ' +student+ ' набрал ' +points[index]+ ' баллов');
});


//Находим студента, набравшего наибольшее количество баллов

var max, maxIndex, i;
points.forEach(function (number, i) {
	if (!max || number > max) {
		max = number;
		maxIndex  = i;
	}
});
console.log('Студент, набравший максимальный балл: ' +students[maxIndex]+ ' (' +max+ ') баллов');


//Увеличиваем баллы Ирине Овчинниковой и Алексею Левенец на 30

points = students.map(function (student, i) {
	if (student === 'Ирина Овчинникова' || student === 'Александр Малов') {
		return points[i] += 30;
	} else {
		return points[i];
	}
});
//console.log(points);


//Выводим top студентов

var studentsAndPointsMax = [];
var studentsMax = students;
var pointsMax = points;
function getTop(topNumber) {
	console.log('Топ ' +topNumber+ ':');
	for (var i = 0, imax = topNumber; i < imax; i++) {
		pointsMax.forEach(function (number, i) {
			if (!max || number > max) {
				max = number;
				maxIndex = i;
			}
		});
		studentsAndPointsMax.push(studentsMax[maxIndex]);
		studentsAndPointsMax.push(pointsMax[maxIndex]);
		console.log(students[maxIndex]+ ' - ' +points[maxIndex]+ ' баллов');
		studentsMax.splice(maxIndex, 1);
		pointsMax.splice(maxIndex, 1);
		max = 0;
	};
};

getTop(3);
//getTop(5);